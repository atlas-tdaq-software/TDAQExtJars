# TDAQExtJars

## tdaq-12-00-00

-   The following packages have been updated to a newer version:
    `io.jsonwebtoken` (from 0.11.2 to **0.12.6**)

-   The following packages have been added:
    `bouncycastle` (version **1.79**)

## tdaq-09-03-00

-   The following packages have been updated to a newer version:    
    `esper` (from 8.5.0 to **8.7.0**) 

-   The following new packages have been added:    
    `io.jsonwebtoken` (version **0.11.2**) 

## tdaq-09-02-01

-   The following packages have been updated to a newer version:    
    `JFreeChart` (from 1.0.16 to **1.5.0**)   
    `jide-oss/common` (from 3.5.8 to **3.7.7**)   
    `com.jhlabs.filters` (**2.0.235-1**)  
    `mysql-connector-java` (from 3.0.16 to **8.0.19**)    
    `oracle.ojdbc` (from 11.2.0.3.0 to **19.3.0.0**)  
    `jldap` (from 2007.10.12 to **2009-10-07**)   
    `esper` (from 8.4.0 to **8.5.0**) 
    
